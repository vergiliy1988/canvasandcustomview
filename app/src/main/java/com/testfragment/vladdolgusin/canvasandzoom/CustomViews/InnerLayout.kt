package com.testfragment.vladdolgusin.canvasandzoom.CustomViews

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.activity_main.view.*

class InnerLayout: FrameLayout {
    private var mScaleGestureDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1.0f

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mScaleGestureDetector = ScaleGestureDetector(context, ScaleListener())
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        mScaleGestureDetector!!.onTouchEvent(ev)
        return false
    }

    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            Log.i("SCALE","DO ${scaleGestureDetector.scaleFactor} - $mScaleFactor ")
            mScaleFactor *= scaleGestureDetector.scaleFactor
            mScaleFactor = Math.max(1f, Math.min(mScaleFactor, 3.0f))
            getChildAt(0).scaleX = mScaleFactor
            getChildAt(0).scaleY = mScaleFactor
            return true
        }
    }
}