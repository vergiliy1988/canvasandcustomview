package com.testfragment.vladdolgusin.canvasandzoom.CustomViews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.annotation.Dimension
import android.support.annotation.Px
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import java.util.*

class CustomViewClock: View {

    private var mPaint: Paint? = null
    private var mWidth = 0
    private var mHeight = 0

    private var radius = 0

    private var paddingView = dpToPx(10)

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    fun init(context: Context, attrs: AttributeSet?, style: Int){
        mPaint = Paint()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        mHeight = getDefaultSize(suggestedMinimumHeight,
                heightMeasureSpec)
        mWidth = getDefaultSize(suggestedMinimumWidth,
                widthMeasureSpec)
        radius = Math.min(mHeight, mWidth) / 2 - 20

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas?) {
        mPaint!!.strokeWidth = 5f
        mPaint!!.color  = Color.RED
        mPaint!!.isAntiAlias = true
        mPaint!!.style = Paint.Style.STROKE
        canvas!!.drawCircle((mWidth / 2 ).toFloat(), (mHeight / 2 ).toFloat(), radius.toFloat(), mPaint!!)

        val calendar = Calendar.getInstance()
        val seconds = calendar.get(Calendar.SECOND)
        val minuts = calendar.get(Calendar.MINUTE)
        val hours = calendar.get(Calendar.HOUR)

        drawArrow(1, seconds, canvas)
        drawArrow(2, minuts, canvas)
        drawArrow(3, hours, canvas)

        postInvalidateDelayed(500)
        invalidate()
        //super.onDraw(canvas)
    }

    private fun drawArrow(type: Int, value: Int, canvas: Canvas?) {
        when (type) {
            1 -> {
                mPaint!!.color = Color.BLACK
                val secondsLine = radius * 0.8
                var angle = (-90 + (value * 6)) * Math.PI / 180
                canvas!!.drawLine((mWidth / 2 ).toFloat(), (mHeight / 2 ).toFloat(), (mWidth / 2 + Math.cos(angle) * secondsLine).toFloat(),(mHeight / 2 + Math.sin(angle) * secondsLine).toFloat(), mPaint!!)
            }
            2 -> {
                mPaint!!.color = Color.GREEN
                mPaint!!.strokeWidth = 7f
                val secondsLine = radius * 0.8
                var angle = (-90 + (value * 6)) * Math.PI / 180
                canvas!!.drawLine((mWidth / 2 ).toFloat(), (mHeight / 2 ).toFloat(), (mWidth / 2 + Math.cos(angle) * secondsLine).toFloat(),(mHeight / 2 + Math.sin(angle) * secondsLine).toFloat(), mPaint!!)
            }
            3 -> {
                mPaint!!.color = Color.YELLOW
                mPaint!!.strokeWidth = 10f
                val secondsLine = radius * 0.6
                var angle = (-90 + (value * 30)) * Math.PI / 180
                canvas!!.drawLine((mWidth / 2 ).toFloat(), (mHeight / 2 ).toFloat(), (mWidth / 2 + Math.cos(angle) * secondsLine).toFloat(),(mHeight / 2 + Math.sin(angle) * secondsLine).toFloat(), mPaint!!)
            }
            else -> {
                mPaint!!.color = Color.BLACK
                val secondsLine = radius * 0.8
                var angle = 90 + (value * 6) * Math.PI / 180
                canvas!!.drawLine((mWidth / 2 ).toFloat(), (mHeight / 2 ).toFloat(), (mWidth / 2 + Math.cos(angle) * secondsLine).toFloat(),(mHeight / 2 + Math.sin(angle) * secondsLine).toFloat(), mPaint!!)
            }
        }
    }


    @Px
    private fun dpToPx(@Dimension(unit = Dimension.DP) dp: Int): Int {
        val resources = resources
        val displayMetrics = resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), displayMetrics).toInt()
    }

}