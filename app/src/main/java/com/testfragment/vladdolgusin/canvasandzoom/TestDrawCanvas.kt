package com.testfragment.vladdolgusin.canvasandzoom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.testfragment.vladdolgusin.canvasandzoom.CustomViews.CustomView
import kotlinx.android.synthetic.main.activity_test_draw_canvas.*

class TestDrawCanvas : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_draw_canvas)

        val customFlightView = findViewById<View>(R.id.custom_view) as CustomView
        customFlightView.setOnClickListener {
            Log.i("CLICK","ALL VIEW")
            customFlightView.getWAndH()
        }

        customFlightView.bindFlight()

        showArc.setOnClickListener {
            intent = Intent(this, CustomArc::class.java)
            startActivity(intent)
        }
    }
}
