package com.testfragment.vladdolgusin.canvasandzoom.CustomViews

import android.content.Context
import android.graphics.*
import android.support.annotation.*
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.testfragment.vladdolgusin.canvasandzoom.R
import com.testfragment.vladdolgusin.canvasandzoom.RoundedRectShadowOwner


class CustomView : LinearLayout, RoundedRectShadowOwner {

    @ColorInt
    private var headerColor: Int = 0
    @ColorInt
    private var headerTextColor: Int = 0
    @ColorInt
    private var cardBackgroundColor: Int = 0
    @ColorInt
    private var primaryTextColor: Int = 0
    @ColorInt
    private var secondaryTextColor: Int = 0
    @ColorInt
    private var dividerColor: Int = 0

    @Px
    private var paddingHorizontal: Int = 0
    @Px
    private var paddingVertical: Int = 0
    @Px
    private var paddingInner: Int = 0
    @Px
    private var dividerHeight: Int = 0
    @Px
    private var cornerRadius: Float = 0f

    @Dimension(unit = Dimension.SP)
    private var primaryTextSize: Float = 0.toFloat()
    @Dimension(unit = Dimension.SP)
    private var secondaryTextSize: Float = 0.toFloat()

    private var paint: Paint? = null
    private var rect: RectF? = null
    private var cardBordersPath: Path? = null

    private var numberTextView: TextView? = null
    private var departureDateTextView: TextView? = null
    private var takeOffTimeTextView: TextView? = null
    private var departureCodeTextView: TextView? = null
    private var departureCityTextView: TextView? = null
    private var departureAirportNameTextView: TextView? = null
    private var landingTimeTextView: TextView? = null
    private var arrivalCodeTextView: TextView? = null
    private var arrivalCityTextView: TextView? = null
    private var arrivalAirportNameTextView: TextView? = null
    private var durationTextView: TextView? = null
    private var planeImageView: ImageView? = null

    private var leftButton: Button? = null
    private var rightButton: Button? = null

    private var counterTextView: TextView? = null


    private var globalCount = 0

    constructor(@NonNull context: Context) : super(context) {
        init(context)
    }

    constructor(@NonNull context: Context, @Nullable attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(@NonNull context: Context, @Nullable attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(@NonNull context: Context) {

        // Prepare colors
        headerColor = ContextCompat.getColor(context, R.color.primary)
        headerTextColor = ContextCompat.getColor(context, R.color.white_text)
        cardBackgroundColor = ContextCompat.getColor(context, R.color.white)
        primaryTextColor = ContextCompat.getColor(context, R.color.primary_text)
        secondaryTextColor = ContextCompat.getColor(context, R.color.secondary_text)
        dividerColor = ContextCompat.getColor(context, R.color.divider)

        // Prepare paddings
        paddingHorizontal = dpToPx(8)
        paddingVertical = dpToPx(8)
        paddingInner = dpToPx(8)
        dividerHeight = dpToPx(1)
        cornerRadius = dpToPx(16).toFloat()

        // Prepare text sizes
        primaryTextSize = 24f
        secondaryTextSize = 14f

        // Prepare drawing objects
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        rect = RectF()
        cardBordersPath = Path()

        // Instantiate, setup and attach child views
        numberTextView = TextView(context)
        numberTextView!!.textSize = primaryTextSize
        numberTextView!!.setTextColor(headerTextColor)
        addView(numberTextView)

        departureDateTextView = TextView(context)
        departureDateTextView!!.textSize = primaryTextSize
        departureDateTextView!!.setTextColor(headerTextColor)
        addView(departureDateTextView)

        takeOffTimeTextView = TextView(context)
        takeOffTimeTextView!!.textSize = primaryTextSize
        takeOffTimeTextView!!.setTextColor(primaryTextColor)
        addView(takeOffTimeTextView)

        departureCodeTextView = TextView(context)
        departureCodeTextView!!.textSize = primaryTextSize
        departureCodeTextView!!.setTextColor(primaryTextColor)
        addView(departureCodeTextView)

        departureCityTextView = TextView(context)
        departureCityTextView!!.textSize = secondaryTextSize
        departureCityTextView!!.setTextColor(secondaryTextColor)
        addView(departureCityTextView)

        departureAirportNameTextView = TextView(context)
        departureAirportNameTextView!!.textSize = secondaryTextSize
        departureAirportNameTextView!!.setTextColor(secondaryTextColor)
        addView(departureAirportNameTextView)

        landingTimeTextView = TextView(context)
        landingTimeTextView!!.textSize = primaryTextSize
        landingTimeTextView!!.setTextColor(primaryTextColor)
        addView(landingTimeTextView)

        arrivalCodeTextView = TextView(context)
        arrivalCodeTextView!!.textSize = primaryTextSize
        arrivalCodeTextView!!.setTextColor(primaryTextColor)
        addView(arrivalCodeTextView)

        arrivalCityTextView = TextView(context)
        arrivalCityTextView!!.textSize = secondaryTextSize
        arrivalCityTextView!!.setTextColor(secondaryTextColor)
        addView(arrivalCityTextView)

        arrivalAirportNameTextView = TextView(context)
        arrivalAirportNameTextView!!.textSize = secondaryTextSize
        arrivalAirportNameTextView!!.setTextColor(secondaryTextColor)
        addView(arrivalAirportNameTextView)

        durationTextView = TextView(context)
        durationTextView!!.textSize = secondaryTextSize
        durationTextView!!.setTextColor(secondaryTextColor)
        addView(durationTextView)


        counterTextView = TextView(context)
       // val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        counterTextView?.apply {
            textSize = secondaryTextSize
            setTextColor(secondaryTextColor)
            text = "0"
         //   layoutParams = params
            gravity = Gravity.CENTER
        }

        addView(counterTextView)

        planeImageView = ImageView(context)
        planeImageView!!.setImageDrawable(VectorDrawableCompat.create(context.resources, R.drawable.ic_android_black_24dp, null))
        addView(planeImageView)

        leftButton = Button(context)
        leftButton!!.textSize = secondaryTextSize
        leftButton!!.setTextColor(secondaryTextColor)
        leftButton!!.text = "munis"
        leftButton!!.setBackgroundColor(Color.CYAN)
        leftButton!!.gravity = Gravity.CENTER
        addView(leftButton)


        rightButton = Button(context)
        rightButton!!.textSize = secondaryTextSize
        rightButton!!.setTextColor(secondaryTextColor)
        rightButton!!.text = "plus"
        rightButton!!.setBackgroundColor(Color.MAGENTA)
        rightButton!!.gravity = Gravity.CENTER
        addView(rightButton)


        leftButton!!.setOnClickListener {
            globalCount--
            counterTextView!!.text = "$globalCount"
        }

        rightButton!!.setOnClickListener {
            globalCount++
            counterTextView!!.text = "$globalCount"
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        Log.i("INNER", "widthMeasureSpec - $widthMeasureSpec, heightMeasureSpec - $heightMeasureSpec")

        var widthMeasureSpec = widthMeasureSpec
        widthMeasureSpec = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)

        // Measure the plane image first, it's kind of anchor for other children
        measureChild(planeImageView, widthMeasureSpec, heightMeasureSpec)

        // Determine text width for the header views and the body views
        val bodyTextWidth = (widthMeasureSpec - planeImageView!!.measuredWidth) / 2 - paddingHorizontal
        val headerTextWidth = widthMeasureSpec / 2 - paddingHorizontal
        val columnThree = widthMeasureSpec / 3 - paddingHorizontal

        // Specify max width for the TextViews
        numberTextView!!.maxWidth = headerTextWidth
        departureDateTextView!!.maxWidth = headerTextWidth
        takeOffTimeTextView!!.maxWidth = headerTextWidth
        departureCodeTextView!!.maxWidth = bodyTextWidth
        departureCityTextView!!.maxWidth = bodyTextWidth
        departureAirportNameTextView!!.maxWidth = bodyTextWidth
        landingTimeTextView!!.maxWidth = bodyTextWidth
        arrivalCodeTextView!!.maxWidth = bodyTextWidth
        arrivalCityTextView!!.maxWidth = bodyTextWidth
        arrivalAirportNameTextView!!.maxWidth = bodyTextWidth

        counterTextView!!.maxWidth = columnThree

        // Measure children
        measureChild(numberTextView, headerTextWidth, heightMeasureSpec)
        measureChild(departureDateTextView, headerTextWidth, heightMeasureSpec)
        measureChild(takeOffTimeTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(departureCodeTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(departureCityTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(departureAirportNameTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(landingTimeTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(arrivalCodeTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(arrivalCityTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(arrivalAirportNameTextView, bodyTextWidth, heightMeasureSpec)
        measureChild(leftButton, columnThree, heightMeasureSpec)
        measureChild(rightButton, columnThree, heightMeasureSpec)
        measureChild(counterTextView, columnThree, heightMeasureSpec)
        measureChild(durationTextView, widthMeasureSpec, heightMeasureSpec)

        // Calculate the height of the whole view
        val heightUsed = paddingVertical +
                numberTextView!!.measuredHeight +
                paddingInner +
                paddingInner +
                takeOffTimeTextView!!.measuredHeight +
                departureCodeTextView!!.measuredHeight +
                departureCityTextView!!.measuredHeight +
                dividerHeight +
                departureAirportNameTextView!!.measuredHeight +
                paddingInner +
                dividerHeight +
                paddingInner +
                durationTextView!!.measuredHeight +
                paddingVertical +
                leftButton!!.measuredHeight

        // Finally, set the measured dimensions to the FlightView
        setMeasuredDimension(widthMeasureSpec, heightUsed)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {

        Log.i("INNER", "left - $left, top - $top, right - $right, bottom - $bottom")
        // Prepare some dimens
        val width = right - left
        val leftBorder = paddingHorizontal
        val rightBorder = width - paddingHorizontal
        val centerHorizontal = width / 2
        val centerVertical = (bottom - top) / 2

        val columnTree = width / 3

        // Counter for the spent height
        var heightUsed = paddingVertical

        // Layout rows one-by-one
        // 1. Header
        numberTextView!!.layout(
                leftBorder,
                heightUsed,
                leftBorder + numberTextView!!.measuredWidth,
                heightUsed + numberTextView!!.measuredHeight
        )
        departureDateTextView!!.layout(
                rightBorder - departureDateTextView!!.measuredWidth,
                heightUsed,
                right,
                heightUsed + departureDateTextView!!.measuredHeight
        )
        heightUsed += numberTextView!!.measuredHeight + paddingInner + paddingInner

        // 2. Take off and landing times
        takeOffTimeTextView!!.layout(
                leftBorder,
                heightUsed,
                leftBorder + takeOffTimeTextView!!.measuredWidth,
                heightUsed + takeOffTimeTextView!!.measuredHeight
        )
        landingTimeTextView!!.layout(
                rightBorder - landingTimeTextView!!.measuredWidth,
                heightUsed,
                rightBorder,
                heightUsed + landingTimeTextView!!.measuredHeight
        )
        heightUsed += takeOffTimeTextView!!.measuredHeight

        // 3. Airports' codes row
        departureCodeTextView!!.layout(
                leftBorder,
                heightUsed,
                leftBorder + departureCodeTextView!!.measuredWidth,
                heightUsed + departureCodeTextView!!.measuredHeight
        )
        arrivalCodeTextView!!.layout(
                rightBorder - arrivalCodeTextView!!.measuredWidth,
                heightUsed,
                rightBorder,
                heightUsed + arrivalCodeTextView!!.measuredHeight
        )
        heightUsed += departureCodeTextView!!.measuredHeight

        // 4. Cities row
        departureCityTextView!!.layout(
                leftBorder,
                heightUsed,
                leftBorder + departureCityTextView!!.measuredWidth,
                heightUsed + departureCityTextView!!.measuredHeight
        )
        arrivalCityTextView!!.layout(
                rightBorder - arrivalCityTextView!!.measuredWidth,
                heightUsed,
                rightBorder,
                heightUsed + arrivalCityTextView!!.measuredHeight
        )
        heightUsed += departureCityTextView!!.measuredHeight + dividerHeight

        // 5. Airports' names row
        departureAirportNameTextView!!.layout(
                leftBorder,
                heightUsed,
                leftBorder + departureAirportNameTextView!!.measuredWidth,
                heightUsed + departureAirportNameTextView!!.measuredHeight
        )
        arrivalAirportNameTextView!!.layout(
                rightBorder - arrivalAirportNameTextView!!.measuredWidth,
                heightUsed,
                rightBorder,
                heightUsed + arrivalAirportNameTextView!!.measuredHeight
        )
        heightUsed += departureAirportNameTextView!!.measuredHeight + paddingInner + dividerHeight + paddingInner

        // 6. Flight duration row
        durationTextView!!.layout(
                centerHorizontal - durationTextView!!.measuredWidth / 2,
                heightUsed,
                centerHorizontal + durationTextView!!.measuredWidth - durationTextView!!.measuredWidth / 2,
                heightUsed + durationTextView!!.measuredHeight
        )
        heightUsed += durationTextView!!.measuredHeight

        // 7. Plane image view
        planeImageView!!.layout(
                centerHorizontal - planeImageView!!.measuredWidth / 2,
                centerVertical - planeImageView!!.measuredHeight / 2,
                centerHorizontal + planeImageView!!.measuredWidth - planeImageView!!.measuredWidth / 2,
                centerVertical + planeImageView!!.measuredHeight - planeImageView!!.measuredHeight / 2
        )

        leftButton!!.layout(
                leftBorder,
                heightUsed,
                columnTree,
                heightUsed + leftButton!!.measuredHeight
        )
        leftButton!!.layoutParams.width = columnTree

        val tempH = ((leftButton!!.measuredHeight - counterTextView!!.measuredHeight) / 2)
        val tempW = (columnTree - counterTextView!!.measuredWidth) / 2

        counterTextView!!.layout(
                columnTree + tempW,
                heightUsed + tempH,
                columnTree + counterTextView!!.measuredWidth + tempW,
                heightUsed +  tempH + counterTextView!!.measuredHeight
        )

        rightButton!!.layout(
                columnTree * 2,
                heightUsed,
                rightBorder ,
                heightUsed + rightButton!!.measuredHeight
        )
        rightButton!!.layoutParams.width = columnTree
        heightUsed += leftButton!!.measuredHeight
    }

    override fun dispatchDraw(canvas: Canvas) {
        val width = canvas.width
        val height = canvas.height

        // Draw card header background
        paint!!.color = headerColor
        val headerHeight = paddingVertical + numberTextView!!.measuredHeight + paddingInner
        rect!!.set(0f, 0f, width.toFloat(), (headerHeight + cornerRadius).toFloat())
        canvas.save()
        canvas.clipRect(0, 0, width, headerHeight)
        canvas.drawRoundRect(
                rect,
                cornerRadius,
                cornerRadius,
                paint
        )
        canvas.restore()

        // Draw card background
        paint!!.color = cardBackgroundColor
        rect!!.set(0f, (headerHeight - cornerRadius).toFloat(), width.toFloat(), height.toFloat())
        canvas.save()
        canvas.clipRect(0, headerHeight, width, height)
        canvas.drawRoundRect(
                rect,
                cornerRadius,
                cornerRadius,
                paint
        )
        canvas.restore()

        // Draw dividers

        paint!!.strokeWidth = dividerHeight.toFloat()
        paint!!.color = dividerColor
        val smallDividerY = departureCityTextView!!.bottom.toFloat() + dividerHeight / 2f + 1f
        canvas.drawLine(
                paddingHorizontal.toFloat(),
                smallDividerY,
                (planeImageView!!.left - paddingInner).toFloat(),
                smallDividerY,
                paint
        )
        canvas.drawLine(
                (planeImageView!!.right + paddingInner).toFloat(),
                smallDividerY,
                (width - paddingHorizontal).toFloat(),
                smallDividerY,
                paint
        )
        val largeDividerY = departureAirportNameTextView!!.bottom.toFloat() + paddingInner.toFloat() + dividerHeight / 2f + 1f
        canvas.drawLine(
                0f,
                largeDividerY,
                width.toFloat(),
                largeDividerY,
                paint
        )

        // Prevent drawing touch feedback outside
        rect!!.set(0f, 0f, width.toFloat(), height.toFloat())
        cardBordersPath!!.addRoundRect(rect, cornerRadius, cornerRadius, Path.Direction.CCW)
        canvas.clipPath(cardBordersPath)

        // Draw children
        super.dispatchDraw(canvas)

    }

    fun bindFlight() {
        numberTextView!!.text = "23"
        departureDateTextView!!.text = "23-12-2009"
        takeOffTimeTextView!!.text = "12-39"
        departureCodeTextView!!.text = "UK"
        departureCityTextView!!.text = "London"
        departureAirportNameTextView!!.text = "Same name airpirt bla bla"
        landingTimeTextView!!.text = "13-50"
        arrivalCodeTextView!!.text = "MSC"
        arrivalCityTextView!!.text = "Moskow"
        arrivalAirportNameTextView!!.text = "Platov"
        durationTextView!!.text = "6h: 56m"
        counterTextView!!.text = "345345359798789798"
    }

    @Px
    private fun dpToPx(@Dimension(unit = Dimension.DP) dp: Int): Int {
        val resources = resources
        val displayMetrics = resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), displayMetrics).toInt()
    }

    @NonNull
    override fun cardBackgroundRect(): Rect {
        return Rect(0, 0, width, height)
    }

    override fun cardCornerRadius(): Float {
        return cornerRadius.toFloat()
    }

    fun getWAndH(){
        this.invalidate()
        val wh = "${counterTextView!!.width}  -  ${counterTextView!!.height}"
        Log.i("INNER VIEW", wh)
    }



}