package com.testfragment.vladdolgusin.canvasandzoom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity(){
    private var mScaleGestureDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView4.setOnClickListener {
            showSnack("Click on Android icon")
        }

        imageView5.setOnClickListener {
            showSnack("Click on Note icon")
        }

        imageView7.setOnClickListener {
            showSnack("Click on Ambrela icon")
        }

        showCanvas.setOnClickListener {
            val intent = Intent(this, TestDrawCanvas::class.java)
            startActivity(intent)
        }
    }


    private fun showSnack(message: String){
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
                .show()
    }
}
