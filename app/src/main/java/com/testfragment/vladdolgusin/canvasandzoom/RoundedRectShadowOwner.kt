package com.testfragment.vladdolgusin.canvasandzoom

import android.graphics.Rect
import android.support.annotation.NonNull

interface RoundedRectShadowOwner {
    @NonNull
    fun cardBackgroundRect(): Rect

    fun cardCornerRadius(): Float
}