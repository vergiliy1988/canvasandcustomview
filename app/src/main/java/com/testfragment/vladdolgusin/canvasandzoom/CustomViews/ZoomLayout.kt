package com.testfragment.vladdolgusin.canvasandzoom.CustomViews

import android.content.Context
import android.graphics.Matrix
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.view.*

class ZoomLayout : FrameLayout {

    private var dX: Float = 0f
    private var dY: Float = 0f

    private var upPoint = false

    private var baseWRoot = 0
    private var baseHRoot = 0

    private var oldXParent = 0
    private var oldYParent = 0

    private var innerLayout: InnerLayout? = null

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        //innerLayout = InnerLayout(context)
        //addView(innerLayout)
        this.setOnTouchListener { v, event ->  true}
    }


    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return if(checkMoveChildView()){
            super.onInterceptTouchEvent(ev)
        }else{
            true
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {

        if (ev.action == MotionEvent.ACTION_POINTER_UP) {
            upPoint = true
        }

        if (ev.action == MotionEvent.ACTION_DOWN) {
            checkMoveChildView(true)
            if (baseWRoot == 0) {
                baseWRoot = parentLayout.width
            }

            if (baseHRoot == 0) {
                baseHRoot = parentLayout.height
            }

            dX = parentLayout.x - ev.rawX
            dY = parentLayout.y - ev.rawY

            upPoint = false
        }

        if (ev.action == MotionEvent.ACTION_MOVE) {
            if (!upPoint &&  parentLayout.scaleX > 1) {
                val realY = parentLayout.x - ((baseWRoot * parentLayout.scaleX - baseWRoot) / 2.0)
                val realX = parentLayout.y - ((baseHRoot * parentLayout.scaleY  - baseHRoot) / 2.0)

                //Log.i("INNER", "X - $realX, Y - $realY")

                var canMoveX = true
                var canMoveY = true

                if (canMoveX) {
                    parentLayout.x = ev.rawX + dX
                }
                if (canMoveY) {
                    parentLayout.y = ev.rawY + dY
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun checkMoveChildView(down: Boolean = false): Boolean{
        return if (down) {
            oldXParent = parentLayout.x.toInt()
            oldYParent = parentLayout.y.toInt()
            true
        }else{
            var deltaX = oldXParent - parentLayout.x.toInt()
            var deltaY = oldYParent - parentLayout.y.toInt()

            if (deltaX < 0) {
                deltaX *= -1
            }

            if (deltaY < 0) {
                deltaY *= -1
            }
            return deltaX <= 5 && deltaY <= 5
        }
    }
}